package ricardo.cidenet.service

import ricardo.cidenet.core.AppException
import ricardo.cidenet.data.model.Employee
import ricardo.cidenet.data.repository.EmployeeRepository
import ricardo.cidenet.data.rq.GetEmployeesRq
import java.lang.Exception

class EmployeeService(private val employeeRepository: EmployeeRepository, private val dropdownService: DropdownService) {

    /*
    * Método que consulta los empleados registrados en la base de datos y retorna los la sublista
    * con las cantidades y filtros requeridos
    */
    fun findAllEmployees(request: GetEmployeesRq): List<Employee> {
        val employeesList = employeeRepository.findAllEmployees()
        request.filters?.let { filters ->
            val filteredList = employeesList.filter { e ->
                val otherNames = if (e.otherNames.isNullOrEmpty()) "" else " ${e.otherNames}"
                val fullName = "${e.name}${otherNames} ${e.surname} ${e.secondSurname}"
                fullName.contains(filters.name.uppercase()) && e.employmentCountry == filters.employmentCountry &&
                        e.documentType == filters.documentType && e.documentNumber.contains(filters.documentNumber) &&
                        e.email.contains(filters.email) && e.status == filters.status
            }
            return getSubList(filteredList, request.skip, request.limit)
        }
        return getSubList(employeesList, request.skip, request.limit)
    }

    /*
    * Método que recibe una lista y retorna una sublista con los parámetros especificados
    */
    private fun getSubList(list: List<Employee>, skip: Int, limit: Int): List<Employee> {
        return if (list.size >= skip) {
            if (list.size >= (skip + limit)) list.subList(skip, skip + limit)
            else list.subList(skip, skip + list.size - skip)
        } else listOf()
    }

    /*
    * Metódo que consulta los correos electrónicos que coinciden con la estructura requerida y
    * retorna el siguiente correo disponible
    */
    fun getValidEmail(name: String, entrySurname: String, employmentCountry: Int): String {
        val similarEmails = employeeRepository.getSimilarEmails(name, entrySurname.trim(), employmentCountry)
        val domain = dropdownService.employmentCountries()[employmentCountry - 1].abbreviation
        val surname = entrySurname.trim().replace(" ", "")
        return if (similarEmails.isNotEmpty()) {
            var id = 1
            var email = ""
            do {
                email = "$name.$surname.$id@cidenet.com.$domain".lowercase()
                id++
            } while (similarEmails.contains(email))
            email
        } else {
            "$name.$surname@cidenet.com.$domain".lowercase()
        }
    }

    /*
    * Método que inserta un nuevo empleado en la base de datos
    */
    fun insertEmployee(employee: Employee) {
        val dbEmployee = employeeRepository.findEmployeeByDocument(employee.documentNumber, employee.documentType)
        if (dbEmployee == null) {
            try {
                employeeRepository.insertEmployee(employee)
            } catch (ignore: Exception) {
                throw AppException(500, "Error al intentar insertar en la base de datos")
            }
        } else {
            throw AppException(409, "Ya existe un empleado con esta combinación de tipo y número de documento")
        }
    }

    /*
    * Método que actualiza la información de un empleado en la base de datos
    */
    fun updateEmployee(employee: Employee) {
        val dbEmployee = employeeRepository.findEmployeeByDocument(employee.documentNumber, employee.documentType)
        if (dbEmployee != null) {
            try {
                employeeRepository.updateEmployee(employee)
            } catch (ignore: Exception) {
                throw AppException(500, "Error al intentar actualizar el empleado en la base de datos")
            }
        } else {
            throw AppException(404, "No se ha encontrado el empleado en la base de datos")
        }
    }

    /*
    * Método que elimina un empleado de la base de datos
    */
    fun deleteEmployeeByDocument(documentNumber: String, documentType: Int) {
        val dbEmployee = employeeRepository.findEmployeeByDocument(documentNumber, documentType)
        if (dbEmployee != null) {
            try {
                employeeRepository.deleteEmployeeByDocument(documentNumber, documentType)
            } catch (ignore: Exception) {
                throw AppException(500, "Error al intentar eliminar el empleado de la base de datos")
            }
        } else {
            throw AppException(404, "No se ha encontrado el empleado en la base de datos")
        }

    }

}
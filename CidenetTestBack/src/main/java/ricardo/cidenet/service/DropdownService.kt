package ricardo.cidenet.service

import ricardo.cidenet.data.DropdownModel

class DropdownService {

    /*
    * Método que retorna el listado de paises de empleo disponibles para mostrar
    * en la lista desplegable de la aplicación
    */
    fun employmentCountries(): List<DropdownModel> =
        listOf(
            DropdownModel(1, "Colombia", "co"),
            DropdownModel(2, "Estados Unidos", "us")
        )

    /*
    * Método que retorna el listado de tipos de coumento disponibles para mostrar
    * en la lista desplegable de la aplicación
    */
    fun documentTypes(): List<DropdownModel> =
        listOf(
            DropdownModel(1, "Cédula de Ciudadanía"),
            DropdownModel(2, "Cédula de Extranjería"),
            DropdownModel(3, "Pasaporte"),
            DropdownModel(4, "Permiso Especial")
        )

    /*
    * Método que retorna el listado de áreas de empleo disponibles para mostrar
    * en la lista desplegable de la aplicación
    */
    fun employmentAreas(): List<DropdownModel> =
        listOf(
            DropdownModel(1, "Administración"),
            DropdownModel(2, "Financiera"),
            DropdownModel(3, "Compras"),
            DropdownModel(4, "Infraestructura"),
            DropdownModel(5, "Operación"),
            DropdownModel(6, "Talento Humano"),
            DropdownModel(7, "Servicios Varios")
        )

}
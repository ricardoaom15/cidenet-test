package ricardo.cidenet.util

import ricardo.cidenet.data.GenericResponse

import javax.ws.rs.core.Response

fun <T> GenericResponse<T>.ok(code: Int = 200): Response = Response.status(code).entity(this).build()

fun <T> GenericResponse<T>.error(code: Int): Response = Response.status(code).entity(this).build()


package ricardo.cidenet

import io.dropwizard.Application
import io.dropwizard.setup.Environment
import org.eclipse.jetty.servlets.CrossOriginFilter
import org.skife.jdbi.v2.DBI
import ricardo.cidenet.core.AppHealthCheck
import ricardo.cidenet.core.CORSResponseFilter
import ricardo.cidenet.data.repository.EmployeeRepository
import ricardo.cidenet.data.repository.EmployeeRepositoryImpl
import ricardo.cidenet.resource.DropdownResource
import ricardo.cidenet.resource.EmployeeResource
import ricardo.cidenet.service.DropdownService
import ricardo.cidenet.service.EmployeeService
import java.util.*
import javax.servlet.DispatcherType

class CidenetTestApplication : Application<CidenetTestConfiguration>() {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            CidenetTestApplication().run(*args)
        }
    }

    override fun run(configuration: CidenetTestConfiguration, environment: Environment) {
        environment.healthChecks().register("app-health-check", AppHealthCheck())

        //Configuración para habilitar CORS y permitir las peticiones por medio REST API
        val allowedOrigins = environment.servlets().addFilter("allowedOrigins", CrossOriginFilter::class.java)
        allowedOrigins.setInitParameter("allowedOrigins", "*")
        allowedOrigins.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin")
        allowedOrigins.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD,PATCH")
        allowedOrigins.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType::class.java), true, "/*")
        environment.jersey().register(CORSResponseFilter::class.java)

        //Instaciación del data source que establece la conexión a la base de datos MySQL
        val dataSource = configuration.getDataSourceFactory()?.build(environment.metrics(), "MySQL")
        val dbi = DBI(dataSource)

        //Se registran los recursos que se implementaron para las peticiones
        val employeeRepository: EmployeeRepository = dbi.onDemand(EmployeeRepositoryImpl::class.java)
        val dropdownService = DropdownService()
        environment.jersey().register(DropdownResource(dropdownService))
        environment.jersey().register(EmployeeResource(EmployeeService(employeeRepository, dropdownService)))
    }
}
package ricardo.cidenet.resource

import com.codahale.metrics.annotation.Timed
import ricardo.cidenet.data.GenericResponse
import ricardo.cidenet.service.DropdownService
import ricardo.cidenet.util.error
import ricardo.cidenet.util.ok
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("dropdown")
@Produces(MediaType.APPLICATION_JSON)
class DropdownResource(private val dropdownService: DropdownService) {

    /*
    * Endpoint que retornal el listado de paises de empleo
    */
    @GET
    @Timed
    @Path("employment-countries")
    fun employmentCountries(): Response {
        val result = dropdownService.employmentCountries()
        val response = GenericResponse(body = result)
        return response.ok()
    }

    /*
    * Endpoint que retornal el listado de tipos de documento
    */
    @GET
    @Timed
    @Path("document-types")
    fun documentTypes(): Response {
        val result = dropdownService.documentTypes()
        val response = GenericResponse(body = result)
        return response.ok()
    }

    /*
    * Endpoint que retornal el listado de áreas de empleo
    */
    @GET
    @Timed
    @Path("employment-areas")
    fun employmentAreas(): Response {
        val result = dropdownService.employmentAreas()
        val response = GenericResponse(body = result)
        return response.ok()
    }
}
package ricardo.cidenet.resource

import com.codahale.metrics.annotation.Timed
import ricardo.cidenet.core.AppException
import ricardo.cidenet.data.GenericResponse
import ricardo.cidenet.data.model.Employee
import ricardo.cidenet.data.rq.GetEmployeesRq
import ricardo.cidenet.service.EmployeeService
import ricardo.cidenet.util.error
import ricardo.cidenet.util.ok
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("employees")
@Produces(MediaType.APPLICATION_JSON)
class EmployeeResource(private val employeeService: EmployeeService) {

    /*
    * Endpoint que retorna el listado de los empleados consultados
    * recibe un request donde se especifica las restricciones para
    * el paginamiento y los filtros
    */
    @POST
    @Timed
    @Path("/get-employees")
    fun findAllEmployees(request: GetEmployeesRq): Response {
        val result = employeeService.findAllEmployees(request)
        val response = GenericResponse(result)
        return response.ok();
    }

    /*
    * Endpoint que retornal el siguiente correo válido disponible
    */
    @GET
    @Timed
    @Path("/get-valid-email")
    fun getValidEmail(@QueryParam("name") name: String, @QueryParam("surname") surname: String, @QueryParam("employmentCountry") employmentCountry: Int): Response {
        val result = employeeService.getValidEmail(name, surname, employmentCountry)
        val response = GenericResponse(body = result)
        return response.ok()
    }

    /*
    * Endpoint para agregar un uempleado
    */
    @POST
    @Timed
    fun insertEmployee(employee: Employee): Response {
        return try {
            employeeService.insertEmployee(employee)
            val response = GenericResponse(body = "Empleado agregado")
            response.ok()
        } catch (e: AppException) {
            val response = GenericResponse<Any>(errorMessage = e.errorMessage)
            response.error(e.errorCode)
        }
    }

    /*
    * Endpoint para modificar un empleado
    */
    @PUT
    @Timed
    fun updateEmployee(employee: Employee): Response {
        return try {
            employeeService.updateEmployee(employee)
            val response = GenericResponse(body = "Empleado modificado")
            response.ok()
        } catch (e: AppException) {
            val response = GenericResponse<Any>(errorMessage = e.errorMessage)
            response.error(e.errorCode)
        }
    }

    /*
    * Endpoint para eliminar un empleado
    */
    @DELETE
    @Timed
    fun deleteEmployeeByDocument(@QueryParam("documentNumber") documentNumber: String, @QueryParam("documentType") documentType: Int): Response {
        return try {
            employeeService.deleteEmployeeByDocument(documentNumber, documentType)
            val response = GenericResponse(body = "Empleado eliminado")
            response.ok()
        } catch (e: AppException) {
            val response = GenericResponse<Any>(errorMessage = e.errorMessage)
            response.error(e.errorCode)
        }
    }
}
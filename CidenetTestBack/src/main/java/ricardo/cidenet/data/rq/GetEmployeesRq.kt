package ricardo.cidenet.data.rq

import ricardo.cidenet.data.model.Employee

data class GetEmployeesRq(
    val skip: Int = 0,
    val limit: Int = 10,
    val filters: Employee? = null
)
package ricardo.cidenet.data

data class GenericResponse<T>(
    val success: Boolean,
    val body: T?,
    val errorMessage: String?
) {
    constructor(body: T) : this(success = true, body = body, errorMessage = null)
    constructor(errorMessage: String) : this(success = false, body = null, errorMessage = errorMessage)
}
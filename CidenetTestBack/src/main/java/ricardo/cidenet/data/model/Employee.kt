package ricardo.cidenet.data.model

import java.sql.Timestamp

//Entidad relacionada a la tabla employee en la base de datos
data class Employee(
    val documentNumber: String = "",
    val documentType: Int = 0,
    val surname: String = "",
    val secondSurname: String = "",
    val name: String = "",
    val otherNames: String? = null,
    val employmentCountry: Int = 0,
    val email: String = "",
    val admissionDate: Timestamp = Timestamp(0),
    val area: Int = 0,
    val status: Int = 1,
    val creationDate: Timestamp = Timestamp(0),
    val updateDate: Timestamp = Timestamp(0)
) {
    companion object {
        const val COLUMN_DOCUMENT_NUMBER: String = "document_number"
        const val COLUMN_DOCUMENT_TYPE: String = "document_type"
        const val COLUMN_SURNAME: String = "surname"
        const val COLUMN_SECOND_SURNAME: String = "second_surname"
        const val COLUMN_NAME: String = "name"
        const val COLUMN_OTHER_NAMES: String = "other_names"
        const val COLUMN_EMPLOYMENT_COUNTRY: String = "employment_country"
        const val COLUMN_EMAIL: String = "email"
        const val COLUMN_ADMISSION_DATE: String = "admission_date"
        const val COLUMN_AREA: String = "area"
        const val COLUMN_STATUS: String = "status"
        const val COLUMN_CREATION_DATE: String = "creation_date"
        const val COLUMN_UPDATE_DATE: String = "update_date"
    }
}
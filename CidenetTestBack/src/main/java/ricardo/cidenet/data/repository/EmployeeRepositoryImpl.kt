package ricardo.cidenet.data.repository

import org.skife.jdbi.v2.sqlobject.CreateSqlObject
import ricardo.cidenet.data.model.Employee
import ricardo.cidenet.data.mysql.dao.EmployeeDao

/*
* Implementacion del contrato EmployeeRepository para la base de datos MySQL
*/
abstract class EmployeeRepositoryImpl : EmployeeRepository {

    @CreateSqlObject
    abstract fun employeeDao(): EmployeeDao

    override fun findAllEmployees(): List<Employee> = employeeDao().findAll()

    override fun findEmployeeByDocument(documentNumber: String, documentType: Int): Employee = employeeDao().findByDocument(documentNumber, documentType)

    override fun getSimilarEmails(name: String, surname: String, employmentCountry: Int): List<String> = employeeDao().findSimilarEmails(name, surname, employmentCountry)

    override fun insertEmployee(employee: Employee) = employeeDao().insert(employee)

    override fun updateEmployee(employee: Employee) = employeeDao().update(employee)

    override fun deleteEmployeeByDocument(documentNumber: String, documentType: Int) = employeeDao().deleteByDocument(documentNumber, documentType)

}
package ricardo.cidenet.data.repository

import ricardo.cidenet.data.model.Employee

/*
* Interface o contrato que define el comportamiento del repositorio de empleados
* indistinto de el motor de base de datos que se use
*/
interface EmployeeRepository {

    fun findAllEmployees(): List<Employee>

    fun findEmployeeByDocument(documentNumber: String, documentType: Int): Employee?

    fun getSimilarEmails(name: String, surname: String, employmentCountry: Int): List<String>

    fun insertEmployee(employee: Employee)

    fun updateEmployee(employee: Employee)

    fun deleteEmployeeByDocument(documentNumber: String, documentType: Int)

}
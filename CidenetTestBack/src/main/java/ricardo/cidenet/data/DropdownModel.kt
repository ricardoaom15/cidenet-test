package ricardo.cidenet.data

data class DropdownModel(
    val id: Int,
    val description: String,
    val abbreviation: String? = null
)
package ricardo.cidenet.data.mysql.mapper

import org.skife.jdbi.v2.StatementContext
import org.skife.jdbi.v2.tweak.ResultSetMapper
import ricardo.cidenet.data.model.Employee
import java.sql.ResultSet

class EmployeeMapper : ResultSetMapper<Employee> {

    override fun map(index: Int, resultSet: ResultSet, ctx: StatementContext): Employee =
        Employee(
            documentNumber = resultSet.getString(Employee.COLUMN_DOCUMENT_NUMBER),
            documentType = resultSet.getInt(Employee.COLUMN_DOCUMENT_TYPE),
            surname = resultSet.getString(Employee.COLUMN_SURNAME),
            secondSurname = resultSet.getString(Employee.COLUMN_SECOND_SURNAME),
            name = resultSet.getString(Employee.COLUMN_NAME),
            otherNames = resultSet.getString(Employee.COLUMN_OTHER_NAMES),
            employmentCountry = resultSet.getInt(Employee.COLUMN_EMPLOYMENT_COUNTRY),
            email = resultSet.getString(Employee.COLUMN_EMAIL),
            admissionDate = resultSet.getTimestamp(Employee.COLUMN_ADMISSION_DATE),
            area = resultSet.getInt(Employee.COLUMN_AREA),
            status = resultSet.getInt(Employee.COLUMN_STATUS),
            creationDate = resultSet.getTimestamp(Employee.COLUMN_CREATION_DATE),
            updateDate = resultSet.getTimestamp(Employee.COLUMN_UPDATE_DATE)
        )

}
package ricardo.cidenet.data.mysql.dao

import org.skife.jdbi.v2.sqlobject.Bind
import org.skife.jdbi.v2.sqlobject.BindBean
import org.skife.jdbi.v2.sqlobject.SqlQuery
import org.skife.jdbi.v2.sqlobject.SqlUpdate
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper
import ricardo.cidenet.data.model.Employee
import ricardo.cidenet.data.mysql.mapper.EmployeeMapper


@RegisterMapper(EmployeeMapper::class)
interface EmployeeDao {

    @SqlQuery("SELECT * FROM employee ORDER BY creation_date DESC")
    fun findAll(): List<Employee>

    @SqlQuery("SELECT * FROM employee WHERE document_number=:documentNumber AND document_type=:documentType")
    fun findByDocument(@Bind("documentNumber") documentNumber: String, @Bind("documentType") documentType: Int): Employee

    @SqlQuery("SELECT email FROM employee WHERE name=:name AND surname=:surname AND employment_country=:employmentCountry")
    fun findSimilarEmails(@Bind("name") name: String, @Bind("surname") surname: String, @Bind("employmentCountry") employmentCountry: Int): List<String>

    @SqlUpdate(
        "INSERT INTO employee(document_number, document_type, surname, second_surname, name, other_names, employment_country, email, admission_date, area, status, creation_date, update_date) " +
                "VALUES(:e.documentNumber, :e.documentType, :e.surname, :e.secondSurname, :e.name, :e.otherNames, :e.employmentCountry, :e.email, :e.admissionDate, :e.area, :e.status, :e.creationDate, :e.updateDate)"
    )
    fun insert(@BindBean("e") employee: Employee)

    @SqlUpdate(
        "UPDATE employee SET document_number=:e.documentNumber, document_type=:e.documentType, surname=:e.surname, second_surname=:e.secondSurname, name=:e.name, " +
                "other_names=:e.otherNames, employment_country=:e.employmentCountry, email=:e.email, admission_date=:e.admissionDate, area=:e.area, status=:e.status, update_date=:e.updateDate " +
                "WHERE document_number=:e.documentNumber AND document_type=:e.documentType"
    )
    fun update(@BindBean("e") employee: Employee)

    @SqlUpdate("DELETE FROM employee WHERE document_number=:documentNumber AND document_type=:documentType")
    fun deleteByDocument(@Bind("documentNumber") documentNumber: String, @Bind("documentType") documentType: Int)

}

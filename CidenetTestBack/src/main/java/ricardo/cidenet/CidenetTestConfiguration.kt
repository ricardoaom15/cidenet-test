package ricardo.cidenet

import com.fasterxml.jackson.annotation.JsonProperty
import io.dropwizard.Configuration
import io.dropwizard.db.DataSourceFactory
import javax.validation.Valid
import javax.validation.constraints.NotNull

class CidenetTestConfiguration : Configuration() {
    private var dataSourceFactory: @Valid @NotNull DataSourceFactory? = DataSourceFactory()

    @JsonProperty(DATABASE)
    fun getDataSourceFactory(): DataSourceFactory? {
        return dataSourceFactory
    }

    @JsonProperty(DATABASE)
    fun setDataSourceFactory(dataSourceFactory: DataSourceFactory?) {
        this.dataSourceFactory = dataSourceFactory
    }

    companion object {
        const val DATABASE: String = "database"
    }

}
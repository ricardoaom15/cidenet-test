package ricardo.cidenet.core

import com.codahale.metrics.health.HealthCheck

class AppHealthCheck : HealthCheck() {

    override fun check(): Result? {
        return Result.healthy()
    }

}
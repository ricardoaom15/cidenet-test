package ricardo.cidenet.core

data class AppException(
    val errorCode: Int,
    val errorMessage: String
) : Exception(errorMessage)
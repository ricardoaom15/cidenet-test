CREATE SCHEMA IF NOT EXISTS `CidenetTestDB` DEFAULT CHARACTER SET utf8;

CREATE TABLE IF NOT EXISTS `CidenetTestDB`.`employee`
(
 `document_number`    varchar(20) NOT NULL ,
 `document_type`      int NOT NULL ,
 `surname`            varchar(20) NOT NULL ,
 `second_surname`     varchar(20) NOT NULL ,
 `name`               varchar(20) NOT NULL ,
 `other_names`        varchar(20) NULL ,
 `employment_country` int NOT NULL ,
 `email`              varchar(300) NOT NULL ,
 `admission_date`     timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(),
 `area`               int NOT NULL ,
 `status`             int NOT NULL ,
 `creation_date`      timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(),
 `update_date`        timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(),
 PRIMARY KEY (`document_number`, `document_type`)
);

INSERT INTO `employee` (`document_number`, `document_type`, `surname`, `second_surname`, `name`, `other_names`, `employment_country`, `email`, `admission_date`, `area`, `status`, `creation_date`, `update_date`) VALUES
('1020030001', 1, 'PEREZ', 'MONTOYA', 'JUAN', 'CARLOS', 1, 'juan.perez@cidenet.com.co', '2021-07-14 21:38:23', 1, 1, '2021-07-14 21:38:23', '2021-07-14 21:38:23'),
('1020030002', 1, 'PEREZ', 'GOMEZ', 'JUAN', 'FELIPE', 1, 'juan.perez.1@cidenet.com.co', '2021-07-14 21:38:23', 1, 1, '2021-07-14 21:38:23', '2021-07-14 21:38:23'),
('1020030003', 1, 'PEREZ', 'JIMENEZ', 'JUAN', 'DAVID', 1, 'juan.perez.2@cidenet.com.co', '2021-07-14 21:38:23', 1, 1, '2021-07-14 21:38:23', '2021-07-14 21:38:23'),
('1020030004', 1, 'PEREZ', 'CALLE', 'JUAN', 'ANDRES', 1, 'juan.perez.5@cidenet.com.co', '2021-07-14 21:38:23', 1, 1, '2021-07-14 21:38:23', '2021-07-14 21:38:23'),
('1020030005', 1, 'DE LA CALLE', 'MORA', 'JUAN', 'GUILLERMO', 1, 'juan.delacalle@cidenet.com.co', '2021-07-14 21:38:23', 1, 1, '2021-07-14 21:38:23', '2021-07-14 21:38:23'),
('1020030006', 1, 'DE LA CALLE', 'MESA', 'JUAN', 'JOSE', 1, 'juan.delacalle.2@cidenet.com.co', '2021-07-14 21:38:23', 1, 1, '2021-07-14 21:38:23', '2021-07-14 21:38:23');


CREATE USER ricardo_cidenet@localhost IDENTIFIED BY 'eED*FFr*yFs5Kls3';
GRANT ALL PRIVILEGES ON `CidenetTestDB` . * TO ricardo_cidenet@localhost;
import 'package:flutter/services.dart';

class UppercaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    final value = newValue.text;
    final regex = RegExp(r'^[a-zA-Z]+$');
    final isValid = regex.hasMatch(value) || value.isEmpty;

    return TextEditingValue(
      text: isValid ? value.toUpperCase() : oldValue.text.toUpperCase(),
      selection: isValid ? newValue.selection : oldValue.selection,
    );
  }
}

class UppercaseSpaceTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    final value = newValue.text;
    final regex = RegExp(r'^[a-zA-Z ]+$');
    final isValid = regex.hasMatch(value) || value.isEmpty;

    return TextEditingValue(
      text: isValid ? value.toUpperCase() : oldValue.text.toUpperCase(),
      selection: isValid ? newValue.selection : oldValue.selection,
    );
  }
}

class DocumentTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    final value = newValue.text;
    final regex = RegExp(r'^[a-zA-Z0-9-]+$');
    final isValid = regex.hasMatch(value) || value.isEmpty;

    return TextEditingValue(
      text: isValid ? value : oldValue.text,
      selection: isValid ? newValue.selection : oldValue.selection,
    );
  }
}

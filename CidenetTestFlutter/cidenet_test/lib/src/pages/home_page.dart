import 'dart:async';

import 'package:cidenet_test/src/data/data.dart';
import 'package:cidenet_test/src/util/text_formaters.dart';
import 'package:flutter/material.dart';

import 'package:cidenet_test/src/services/services.dart';
import 'package:cidenet_test/src/widgets/employee_card.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ScrollController _scrollController = ScrollController();

  late EmployeeService employeeService;
  late DropdownService dropdownService;
  bool _showFilters = false;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        Timer(Duration(seconds: 1), () {
          /* _scrollController.animateTo(_scrollController.position.pixels - 10,
              duration: Duration(milliseconds: 500),
              curve: Curves.fastOutSlowIn); */
          employeeService.loadMoreEmployees();
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    employeeService = Provider.of<EmployeeService>(context);
    dropdownService = Provider.of<DropdownService>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Cidenet Test'),
      ),
      body: Column(
        children: [
          SwitchListTile(
              title: Text('Mostrar filtros'),
              value: _showFilters,
              onChanged: (value) {
                final now = DateTime.now().millisecondsSinceEpoch;
                if (value && employeeService.filters == null) {
                  employeeService.filters = Employee(
                      admissionDate: now, creationDate: now, updateDate: now);
                }
                setState(() {
                  _showFilters = value;
                });
              }),
          _renderContent()
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          final now = DateTime.now().millisecondsSinceEpoch;
          employeeService.isUpdating = false;
          employeeService.selectedEmployee =
              Employee(admissionDate: now, creationDate: now, updateDate: now);
          Navigator.pushNamed(context, 'save');
        },
      ),
    );
  }

  Widget _renderContent() {
    if (_showFilters) {
      return Expanded(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
            child: _renderFilterForm(),
          ),
        ),
      );
    } else {
      return Expanded(
        child: RefreshIndicator(
          onRefresh: () {
            employeeService.refresh();
            return Future.delayed(Duration(milliseconds: 300));
          },
          child: ListView.builder(
              controller: _scrollController,
              itemCount: employeeService.employees.length + 1,
              itemBuilder: (BuildContext context, int index) => GestureDetector(
                    child: _renderItemList(index),
                    onTap: () => _showBottomSheet(context, index),
                  )),
        ),
      );
    }
  }

  Column _renderFilterForm() {
    return Column(
      children: [
        TextField(
          textCapitalization: TextCapitalization.characters,
          inputFormatters: [UppercaseSpaceTextFormatter()],
          onChanged: (value) {
            employeeService.filters!.name = value;
          },
          decoration: _getTextFieldDecoration('Nombre', Icons.perm_identity),
        ),
        Divider(),
        DropdownButtonFormField<int>(
          value: employeeService.filters!.employmentCountry,
          items: _buildDropdownItems(dropdownService.employmentCountries),
          onChanged: (value) {
            employeeService.filters!.employmentCountry = value!;
          },
          decoration:
              _getTextFieldDecoration('País del empleo', Icons.flag_outlined),
        ),
        Divider(),
        DropdownButtonFormField<int>(
          value: employeeService.filters!.documentType,
          items: _buildDropdownItems(dropdownService.documentTypes),
          onChanged: (value) => employeeService.filters!.documentType = value!,
          decoration: _getTextFieldDecoration(
              'Tipo de identificación', Icons.badge_outlined),
        ),
        Divider(),
        TextField(
          inputFormatters: [DocumentTextFormatter()],
          onChanged: (value) => employeeService.filters!.documentNumber = value,
          decoration: _getTextFieldDecoration(
              'Número de identificación', Icons.badge_outlined),
        ),
        Divider(),
        TextField(
          keyboardType: TextInputType.emailAddress,
          onChanged: (value) => employeeService.filters!.email = value,
          decoration: _getTextFieldDecoration(
              'Correo electrónico', Icons.email_outlined),
        ),
        Divider(),
        SwitchListTile(
            title: Text('Estado'),
            value: employeeService.filters!.status,
            onChanged: (value) {
              setState(() {
                employeeService.filters!.status = value;
              });
            }),
        Row(
          children: [
            Expanded(
                child: ElevatedButton(
              child: Text('Buscar'),
              onPressed: () {
                employeeService.isFiltering = true;
                employeeService.refresh();
                setState(() {
                  _showFilters = false;
                });
              },
            )),
          ],
        ),
        Row(
          children: [
            Expanded(
                child: ElevatedButton(
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.red)),
              child: Text('Limpiar filtros'),
              onPressed: () {
                employeeService.filters = null;
                employeeService.isFiltering = false;
                employeeService.refresh();
                setState(() {
                  _showFilters = false;
                });
              },
            )),
          ],
        )
      ],
    );
  }

  _getTextFieldDecoration(String label, IconData icon) {
    return InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
        labelText: label,
        icon: Icon(icon));
  }

  List<DropdownMenuItem<int>> _buildDropdownItems(List<DropdownModel> options) {
    List<DropdownMenuItem<int>> list = [];
    options.forEach((option) {
      list.add(
          DropdownMenuItem(child: Text(option.description), value: option.id));
    });
    return list;
  }

  Widget _renderItemList(int index) {
    if (index == employeeService.employees.length) {
      if (employeeService.isLoading) {
        return Container(
          margin: EdgeInsets.only(top: 5),
          alignment: Alignment.center,
          child: CircularProgressIndicator(),
        );
      } else {
        return SizedBox(height: 40);
      }
    } else {
      return EmployeeCard(employee: employeeService.employees[index]);
    }
  }

  void _showBottomSheet(BuildContext context, int index) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(height: 10),
              ListTile(
                leading: Icon(Icons.edit),
                title: Text('Editar empleado'),
                onTap: () {
                  employeeService.isUpdating = true;
                  employeeService.selectedEmployee =
                      employeeService.employees[index].copy();
                  employeeService.selectedEmployee.updateDate =
                      DateTime.now().millisecondsSinceEpoch;
                  Navigator.of(context).pop();
                  Navigator.pushNamed(context, 'save');
                },
              ),
              ListTile(
                leading: Icon(Icons.delete),
                title: Text('Eliminar empleado'),
                onTap: () {
                  Navigator.of(context).pop();
                  _showDeleteAlert(context, index);
                },
              ),
              SizedBox(height: 20)
            ],
          );
        });
  }

  void _showDeleteAlert(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            title: Text('Eliminar empleado'),
            content: Text('¿Está seguro que desea eliminar el empleado?'),
            actions: [
              TextButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text('Cancelar')),
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    _deleteEmployee(context, index);
                  },
                  child: Text('Eliminar'))
            ],
          );
        });
  }

  void _deleteEmployee(BuildContext context, int index) {
    employeeService.deleteEmploye(index).then((value) =>
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            duration: Duration(seconds: 2),
            content: Text('Empleado eliminado'))));
  }
}

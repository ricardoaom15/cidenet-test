import 'dart:async';

import 'package:cidenet_test/src/data/data.dart';
import 'package:cidenet_test/src/providers/employee_form_provider.dart';
import 'package:cidenet_test/src/services/services.dart';
import 'package:cidenet_test/src/util/text_formaters.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class EmployeeSavePage extends StatefulWidget {
  @override
  _EmployeeSavePageState createState() => _EmployeeSavePageState();
}

class _EmployeeSavePageState extends State<EmployeeSavePage> {
  late DropdownService dropdownService;
  late EmployeeService employeeService;

  @override
  Widget build(BuildContext context) {
    dropdownService = Provider.of<DropdownService>(context);
    employeeService = Provider.of<EmployeeService>(context);

    return ChangeNotifierProvider(
      create: (_) => EmployeeFormProvider(employeeService.selectedEmployee),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
              employeeService.isUpdating ? 'Actualizando' : 'Nuevo empleado'),
        ),
        body: SingleChildScrollView(
          //keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
          child: _EmployeeeForm(dropdownService, employeeService,
              TextEditingController(), TextEditingController()),
        ),
      ),
    );
  }
}

class _EmployeeeForm extends StatelessWidget {
  final DropdownService dropdownService;
  final EmployeeService employeeService;

  final TextEditingController _inputEmailController;
  final TextEditingController _inputDateController;

  const _EmployeeeForm(this.dropdownService, this.employeeService,
      this._inputEmailController, this._inputDateController);

  @override
  Widget build(BuildContext context) {
    final EmployeeFormProvider employeeFormProvider =
        Provider.of<EmployeeFormProvider>(context);
    final Employee employee = employeeFormProvider.employee;
    _inputEmailController.text = employee.email;
    _inputDateController.text =
        _buildStringDate(employee.admissionDate, 'dd-MM-yyyy');
    return Container(
      child: Form(
        key: employeeFormProvider.formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
          child: Column(
            children: [
              TextFormField(
                initialValue: employee.surname,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                textCapitalization: TextCapitalization.characters,
                inputFormatters: [UppercaseSpaceTextFormatter()],
                maxLength: 20,
                onChanged: (value) {
                  employee.surname = value;
                  _buildEmail(employee);
                },
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'El primer apellido es obligatorio';
                  }
                },
                decoration: _getTextFieldDecoration(
                    'Primer apellido', Icons.perm_identity),
              ),
              Divider(),
              TextFormField(
                initialValue: employee.secondSurname,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                textCapitalization: TextCapitalization.characters,
                inputFormatters: [UppercaseTextFormatter()],
                maxLength: 20,
                onChanged: (value) => employee.secondSurname = value,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'El segundo apellido es obligatorio';
                  }
                },
                decoration: _getTextFieldDecoration(
                    'Segundo Apellido', Icons.perm_identity),
              ),
              Divider(),
              TextFormField(
                initialValue: employee.name,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                textCapitalization: TextCapitalization.characters,
                inputFormatters: [UppercaseTextFormatter()],
                maxLength: 20,
                onChanged: (value) {
                  employee.name = value;
                  _buildEmail(employee);
                },
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'El primer nombre es obligatorio';
                  }
                },
                decoration: _getTextFieldDecoration(
                    'Primer nombre', Icons.perm_identity),
              ),
              Divider(),
              TextFormField(
                initialValue: employee.otherNames,
                textCapitalization: TextCapitalization.characters,
                inputFormatters: [UppercaseTextFormatter()],
                maxLength: 50,
                onChanged: (value) => employee.otherNames = value,
                decoration: _getTextFieldDecoration(
                    'Otros nombres', Icons.perm_identity),
              ),
              Divider(),
              DropdownButtonFormField<int>(
                value: employee.employmentCountry,
                items: _buildDropdownItems(dropdownService.employmentCountries),
                onChanged: (value) {
                  employee.employmentCountry = value!;
                  _buildEmail(employee);
                },
                decoration: _getTextFieldDecoration(
                    'País del empleo', Icons.flag_outlined),
              ),
              Divider(),
              DropdownButtonFormField<int>(
                value: employee.documentType,
                items: _buildDropdownItems(dropdownService.documentTypes),
                onChanged: (value) => employee.documentType = value!,
                decoration: _getTextFieldDecoration(
                    'Tipo de identificación', Icons.badge_outlined),
              ),
              Divider(),
              TextFormField(
                initialValue: employee.documentNumber,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                inputFormatters: [DocumentTextFormatter()],
                maxLength: 20,
                onChanged: (value) => employee.documentNumber = value,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'El número de identificación es obligatorio';
                  }
                },
                decoration: _getTextFieldDecoration(
                    'Número de identificación', Icons.badge_outlined),
              ),
              Divider(),
              TextFormField(
                controller: _inputEmailController,
                maxLength: 300,
                enabled: false,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                onChanged: (value) => employee.email = value,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'El correo electrónico es obligatorio';
                  }
                },
                decoration: _getTextFieldDecoration(
                    'Correo electrónico', Icons.email_outlined),
              ),
              Divider(),
              TextFormField(
                enableInteractiveSelection: false,
                controller: _inputDateController,
                onTap: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                  _selectDate(context, employee);
                },
                decoration: _getTextFieldDecoration(
                    'Fecha ingreso', Icons.calendar_today_outlined),
              ),
              Divider(),
              DropdownButtonFormField<int>(
                value: employee.area,
                items: _buildDropdownItems(dropdownService.employmentAreas),
                onChanged: (value) => employee.area = value!,
                decoration: _getTextFieldDecoration(
                    'Área de empleo', Icons.domain_outlined),
              ),
              Divider(),
              TextFormField(
                initialValue: employee.status == 1 ? 'Activo' : 'Inactivo',
                enabled: false,
                decoration: _getTextFieldDecoration('Estado', Icons.check),
              ),
              Divider(),
              if (!employeeService.isUpdating)
                TextFormField(
                  initialValue: _buildStringDate(
                      employee.creationDate, 'dd-MM-yyyy HH:mm:ss'),
                  enabled: false,
                  decoration: _getTextFieldDecoration(
                      'Fecha y Hora de Registro',
                      Icons.calendar_today_outlined),
                ),
              if (employeeService.isUpdating)
                TextFormField(
                  initialValue: _buildStringDate(
                      employee.updateDate, 'dd-MM-yyyy HH:mm:ss'),
                  enabled: false,
                  decoration: _getTextFieldDecoration(
                      'Fecha y Hora de Actualización',
                      Icons.calendar_today_outlined),
                ),
              Divider(),
              ElevatedButton(
                  style: ButtonStyle(),
                  child: Text('Guardar'),
                  onPressed: () =>
                      _saveEmployee(employeeFormProvider, context)),
              SizedBox(height: 50)
            ],
          ),
        ),
      ),
    );
  }

  _getTextFieldDecoration(String label, IconData icon) {
    return InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
        labelText: label,
        icon: Icon(icon));
  }

  List<DropdownMenuItem<int>> _buildDropdownItems(List<DropdownModel> options) {
    List<DropdownMenuItem<int>> list = [];
    options.forEach((option) {
      list.add(
          DropdownMenuItem(child: Text(option.description), value: option.id));
    });
    return list;
  }

  String _buildStringDate(int date, String format) {
    final DateFormat formatter = DateFormat(format);
    print('date: $date');
    final DateTime dateTime = (date != 0)
        ? DateTime.fromMillisecondsSinceEpoch(date)
        : DateTime.now();
    print('dateTime: $dateTime');
    return formatter.format(dateTime);
  }

  void _selectDate(BuildContext context, Employee employee) {
    final now = DateTime.now();
    showDatePicker(
            context: context,
            initialDate: now,
            firstDate: DateTime(now.year, now.month - 1, now.day),
            lastDate: now)
        .then((datePicked) {
      if (datePicked != null) {
        print('picked');
        employee.admissionDate = datePicked.millisecondsSinceEpoch;
        final strDate =
            _buildStringDate(datePicked.millisecondsSinceEpoch, 'dd-MM-yyyy');
        _inputDateController.text = strDate;
      }
    });
  }

  void _buildEmail(Employee employee) {
    if (employee.name.isNotEmpty && employee.surname.isNotEmpty) {
      employeeService.getValidEmail(employee).then((value) {
        employee.email = value;
        _inputEmailController.text = value;
      });
    }
  }

  void _saveEmployee(
      EmployeeFormProvider employeeFormProvider, BuildContext context) {
    if (employeeFormProvider.isValidForm()) {
      employeeService.saveEmployee(employeeFormProvider.employee).then((value) {
        Navigator.of(context).pop();
      });
    }
  }
}

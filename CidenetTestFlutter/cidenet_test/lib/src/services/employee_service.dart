import 'package:cidenet_test/src/util/constants.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:cidenet_test/src/data/data.dart';

const int LIMIT = 10;

class EmployeeService extends ChangeNotifier {
  final String _baseUrl = '$apiUrl/employees';
  final headers = {
    "Accept": "application/json",
    "content-type": "application/json"
  };

  final List<Employee> employees = [];
  bool isLoading = true;

  int skip = 0;
  int lastSkip = -1;
  bool requestNextPage = false;

  bool isFiltering = false;
  Employee? filters;

  late Employee selectedEmployee;
  bool isUpdating = false;

  EmployeeService() {
    refresh();
  }

  void refresh() {
    clearEmployees();
    loadEmployees();
  }

  void clearEmployees() {
    this.lastSkip = -1;
    this.skip = 0;
    this.employees.clear();
    notifyListeners();
  }

  void loadEmployees() async {
    this.isLoading = true;
    notifyListeners();

    final url = Uri.parse('$_baseUrl/get-employees');
    final body = GetEmployeesRq(
            skip: this.skip,
            limit: LIMIT,
            filters: isFiltering ? filters : null)
        .toJson();
    final response = await http.post(url, headers: headers, body: body);
    final result =
        GetEmployeeListRs.fromMap(json.decode(utf8.decode(response.bodyBytes)));

    this.requestNextPage = result.body.length == LIMIT;
    if (this.skip != this.lastSkip) {
      this.employees.addAll(result.body);
    }

    this.isLoading = false;
    notifyListeners();
  }

  void loadMoreEmployees() {
    this.isLoading = true;
    notifyListeners();
    this.lastSkip = this.skip;
    if (this.requestNextPage) {
      this.skip += LIMIT;
    }
    Future.delayed(Duration(seconds: 1)).then((value) => loadEmployees());
  }

  Future<String> getValidEmail(Employee employee) async {
    Map<String, String> queryParams = {
      'name': employee.name,
      'surname': employee.surname,
      'employmentCountry': employee.employmentCountry.toString()
    };
    final url = Uri.parse(
        '$_baseUrl/get-valid-email?${Uri(queryParameters: queryParams).query}');
    final response = await http.get(url);
    final result =
        StringRs.fromMap(json.decode(utf8.decode(response.bodyBytes)));
    return result.body!;
  }

  Future<String> saveEmployee(Employee employee) async {
    final url = Uri.parse(_baseUrl);
    final body = employee.toJson();
    final response = await (isUpdating
        ? http.put(url, headers: headers, body: body)
        : http.post(url, headers: headers, body: body));
    print(response.body);
    final result =
        StringRs.fromMap(json.decode(utf8.decode(response.bodyBytes)));
    refresh();
    return result.body!;
  }

  Future<String> deleteEmploye(int index) async {
    Map<String, String> queryParams = {
      'documentNumber': employees[index].documentNumber,
      'documentType': employees[index].documentType.toString()
    };
    final url =
        Uri.parse('$_baseUrl?${Uri(queryParameters: queryParams).query}');
    final response = await http.delete(url);
    final result =
        StringRs.fromMap(json.decode(utf8.decode(response.bodyBytes)));
    this.employees.removeAt(index);
    notifyListeners();
    return result.body!;
  }
}

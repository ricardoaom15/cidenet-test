import 'package:cidenet_test/src/util/constants.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:cidenet_test/src/data/data.dart';

class DropdownService extends ChangeNotifier {
  final String _baseUrl = '$apiUrl/dropdown';
  final headers = {
    "Accept": "application/json",
    "content-type": "application/json"
  };

  final List<DropdownModel> employmentCountries = [];
  final List<DropdownModel> documentTypes = [];
  final List<DropdownModel> employmentAreas = [];

  DropdownService() {
    loadEmploymentCountries();
    loadDocumentTypes();
    loadEmploymentAreas();
  }

  void loadEmploymentCountries() async {
    final url = Uri.parse('$_baseUrl/employment-countries');
    final response = await http.get(url);
    final result =
        GetDropdownListRs.fromMap(json.decode(utf8.decode(response.bodyBytes)));
    this.employmentCountries.addAll(result.body);
    notifyListeners();
  }

  void loadDocumentTypes() async {
    final url = Uri.parse('$_baseUrl/document-types');
    final response = await http.get(url);
    final result =
        GetDropdownListRs.fromMap(json.decode(utf8.decode(response.bodyBytes)));
    this.documentTypes.addAll(result.body);
    notifyListeners();
  }

  void loadEmploymentAreas() async {
    final url = Uri.parse('$_baseUrl/employment-areas');
    final response = await http.get(url);
    final result =
        GetDropdownListRs.fromMap(json.decode(utf8.decode(response.bodyBytes)));
    this.employmentAreas.addAll(result.body);
    notifyListeners();
  }
}

import 'dart:convert';

class Employee {
  String documentNumber;
  int documentType;
  String surname;
  String secondSurname;
  String name;
  String? otherNames;
  int employmentCountry;
  String email;
  int admissionDate;
  int area;
  bool status;
  int creationDate;
  int updateDate;

  Employee({
    this.documentNumber = '',
    this.documentType = 1,
    this.surname = '',
    this.secondSurname = '',
    this.name = '',
    this.otherNames,
    this.employmentCountry = 1,
    this.email = '',
    this.admissionDate = 0,
    this.area = 1,
    this.status = true,
    this.creationDate = 0,
    this.updateDate = 0,
  });

  factory Employee.fromJson(String str) => Employee.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Employee.fromMap(Map<String, dynamic> json) => Employee(
        documentNumber: json["documentNumber"],
        documentType: json["documentType"],
        surname: json["surname"],
        secondSurname: json["secondSurname"],
        name: json["name"],
        otherNames: json["otherNames"],
        employmentCountry: json["employmentCountry"],
        email: json["email"],
        admissionDate: json["admissionDate"],
        area: json["area"],
        status: json["status"] == 1,
        creationDate: json["creationDate"],
        updateDate: json["updateDate"],
      );

  Map<String, dynamic> toMap() => {
        "documentNumber": documentNumber,
        "documentType": documentType,
        "surname": surname,
        "secondSurname": secondSurname,
        "name": name,
        "otherNames": otherNames,
        "employmentCountry": employmentCountry,
        "email": email,
        "admissionDate": admissionDate,
        "area": area,
        "status": status ? 1 : 0,
        "creationDate": creationDate,
        "updateDate": updateDate,
      };

  Employee copy() => Employee(
      documentNumber: this.documentNumber,
      documentType: this.documentType,
      surname: this.surname,
      secondSurname: this.secondSurname,
      name: this.name,
      otherNames: this.otherNames,
      employmentCountry: this.employmentCountry,
      email: this.email,
      admissionDate: this.admissionDate,
      area: this.area,
      status: this.status,
      creationDate: this.creationDate,
      updateDate: this.updateDate);
}

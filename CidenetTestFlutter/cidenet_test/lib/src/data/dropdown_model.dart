import 'dart:convert';

class DropdownModel {
  int id;
  String description;
  String? abbreviation;

  DropdownModel({
    required this.id,
    required this.description,
    this.abbreviation,
  });

  factory DropdownModel.fromJson(String str) =>
      DropdownModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory DropdownModel.fromMap(Map<String, dynamic> json) => DropdownModel(
        id: json["id"],
        description: json["description"],
        abbreviation: json["abbreviation"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "description": description,
        "abbreviation": abbreviation,
      };
}

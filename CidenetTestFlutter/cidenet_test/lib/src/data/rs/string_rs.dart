import 'dart:convert';

class StringRs {
  bool success;
  String? body;
  String? errorMessage;

  StringRs({required this.success, required this.body, this.errorMessage});

  factory StringRs.fromJson(String str) => StringRs.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory StringRs.fromMap(Map<String, dynamic> json) => StringRs(
        success: json["success"],
        body: json["body"],
        errorMessage: json["errorMessage"],
      );

  Map<String, dynamic> toMap() => {
        "success": success,
        "body": body,
        "errorMessage": errorMessage,
      };
}

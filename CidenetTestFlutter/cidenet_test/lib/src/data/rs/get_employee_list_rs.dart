import 'dart:convert';

import 'package:cidenet_test/src/data/data.dart';

class GetEmployeeListRs {
  bool success;
  List<Employee> body;
  String? errorMessage;

  GetEmployeeListRs(
      {required this.success, required this.body, this.errorMessage});

  factory GetEmployeeListRs.fromJson(String str) =>
      GetEmployeeListRs.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory GetEmployeeListRs.fromMap(Map<String, dynamic> json) =>
      GetEmployeeListRs(
        success: json["success"],
        body: List<Employee>.from(json["body"].map((x) => Employee.fromMap(x))),
        errorMessage: json["errorMessage"],
      );

  Map<String, dynamic> toMap() => {
        "success": success,
        "body": List<dynamic>.from(body.map((x) => x.toMap())),
        "errorMessage": errorMessage,
      };
}

import 'dart:convert';

import 'package:cidenet_test/src/data/data.dart';

class GetDropdownListRs {
  bool success;
  List<DropdownModel> body;
  String? errorMessage;

  GetDropdownListRs(
      {required this.success, required this.body, this.errorMessage});

  factory GetDropdownListRs.fromJson(String str) =>
      GetDropdownListRs.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory GetDropdownListRs.fromMap(Map<String, dynamic> json) =>
      GetDropdownListRs(
        success: json["success"],
        body: List<DropdownModel>.from(
            json["body"].map((x) => DropdownModel.fromMap(x))),
        errorMessage: json["errorMessage"],
      );

  Map<String, dynamic> toMap() => {
        "success": success,
        "body": List<dynamic>.from(body.map((x) => x.toMap())),
        "errorMessage": errorMessage,
      };
}

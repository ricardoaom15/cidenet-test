import 'dart:convert';

import 'package:cidenet_test/src/data/data.dart';

class GetEmployeesRq {
  int skip;
  int limit;
  Employee? filters;

  GetEmployeesRq({
    required this.skip,
    required this.limit,
    this.filters,
  });

  factory GetEmployeesRq.fromJson(String str) =>
      GetEmployeesRq.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory GetEmployeesRq.fromMap(Map<String, dynamic> json) => GetEmployeesRq(
        skip: json["skip"],
        limit: json["limit"],
        filters: Employee.fromMap(json["filters"]),
      );

  Map<String, dynamic> toMap() => {
        "skip": '$skip',
        "limit": '$limit',
        "filters": filters != null ? filters!.toMap() : null,
      };
}

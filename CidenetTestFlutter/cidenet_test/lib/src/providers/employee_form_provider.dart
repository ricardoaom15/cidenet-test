import 'package:flutter/material.dart';

import 'package:cidenet_test/src/data/data.dart';

class EmployeeFormProvider extends ChangeNotifier {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  Employee employee;

  EmployeeFormProvider(this.employee);

  bool isValidForm() {
    return formKey.currentState?.validate() ?? false;
  }

  void updateEmail(String email) {
    employee.email = email;
    notifyListeners();
  }
}

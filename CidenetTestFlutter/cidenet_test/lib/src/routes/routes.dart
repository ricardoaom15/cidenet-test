import 'package:flutter/material.dart';

import 'package:cidenet_test/src/pages/employee_save_page.dart';
import 'package:cidenet_test/src/pages/home_page.dart';

Map<String, WidgetBuilder> getApplicationRoutes() => <String, WidgetBuilder>{
      'home': (BuildContext context) => HomePage(),
      'save': (BuildContext context) => EmployeeSavePage()
    };

import 'package:cidenet_test/src/data/data.dart';
import 'package:flutter/material.dart';

class EmployeeCard extends StatelessWidget {
  final Employee employee;

  const EmployeeCard({Key? key, required this.employee}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final name =
        (employee.otherNames != null && employee.otherNames!.isNotEmpty)
            ? '${employee.name} ${employee.otherNames}'
            : '${employee.name}';
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
      child: Column(
        children: [
          ListTile(
            leading: Icon(Icons.person),
            title: Text('$name ${employee.surname} ${employee.secondSurname}'),
            subtitle: Text(employee.email),
            trailing: Text(employee.status ? 'Activo' : 'Inactivo'),
          ),
        ],
      ),
    );
  }
}

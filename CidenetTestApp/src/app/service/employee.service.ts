import { HttpClient, HttpErrorResponse, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators'
import { environment } from 'src/environments/environment';
import { GenericResponse } from '../data/generic-response';
import { Employee } from '../data/model/employee';
import { GetEmployeesRq } from '../data/rq/get-employees-rq';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private baseURL = `${environment.apiURL}/employees`

  public currentEmployee: Employee = undefined;

  constructor(private http: HttpClient) { }

  /*
   * Método que hace el request a los servicios para   
   * obtener el listado de empleados con los filtros y cantidades especificadas
   */
  findAllEmployees(request: GetEmployeesRq): Observable<Employee[]> {
    return this.http.post<GenericResponse<Employee[]>>(`${this.baseURL}/get-employees`, request)
      .pipe(
        map(response => {
          if (response.success) return response.body;
          else return [];
        }),
        catchError((httpError: HttpErrorResponse) => {
          return throwError(httpError)
        })
      );
  }

  /*
   * Método que hace el request a los servicios para   
   * obtener el siguiente email valido segun la estructura especificada
   */
  getValidEmail(name: string, surname: string, employmentCountry: number): Observable<string> {
    let params = new HttpParams()
      .set('name', name)
      .set('surname', surname)
      .set('employmentCountry', employmentCountry);
    return this.http.get<GenericResponse<string>>(`${this.baseURL}/get-valid-email`, { params: params })
      .pipe(
        map(response => {
          if (response.success) return response.body;
          else throw new Error(response.errorMessage);
        }),
        catchError((httpError: HttpErrorResponse) => {
          return throwError(httpError.error.errorMessage)
        })
      );
  }

  /*
   * Método que hace el request a los servicios para   
   * crear un nuevo empleado o actualizar uno existente
   */
  saveEmployee(isUpdate: boolean, employee: Employee): Observable<string> {
    let request: Observable<GenericResponse<string>>;
    if (isUpdate) {
      request = this.http.put<GenericResponse<string>>(this.baseURL, employee);
    } else {
      request = this.http.post<GenericResponse<string>>(this.baseURL, employee);
    }
    return request
      .pipe(
        map(response => {
          if (response.success) return response.body;
          else throw new Error(response.errorMessage);
        }),
        catchError((httpError: HttpErrorResponse) => {
          return throwError(httpError.error.errorMessage)
        })
      );
  }

  /*
   * Método que hace el request a los servicios para   
   * eliminar un empleado especifico
   */
  deleteEmployeeByDocument(documentNumber: string, documentType: number): Observable<string> {
    let params = new HttpParams()
      .set('documentNumber', documentNumber)
      .set('documentType', documentType);
    return this.http.delete<GenericResponse<string>>(this.baseURL, { params: params })
      .pipe(
        map(response => {
          if (response.success) return response.body;
          else throw new Error(response.errorMessage);
        }),
        catchError((httpError: HttpErrorResponse) => {
          return throwError(httpError.error.errorMessage)
        })
      );
  }

}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'
import { environment } from 'src/environments/environment';
import { DropdownModel } from '../data/dropdown-model';
import { GenericResponse } from '../data/generic-response';

@Injectable({
  providedIn: 'root'
})
export class DropdownService {
  private baseURL = `${environment.apiURL}/dropdown`

  public employmentCountriesList: DropdownModel[] = [];
  public documentTypesList: DropdownModel[] = [];
  public employmentAreasList: DropdownModel[] = [];

  constructor(private http: HttpClient) { }

  /*
   * Método que hace el request a los servicios para   
   * obtener el listado de los paises de empleo
   */
  employmentCountries(): Observable<DropdownModel[]> {
    return this.http.get<GenericResponse<DropdownModel[]>>(`${this.baseURL}/employment-countries`)
      .pipe(
        map(response => {
          if (response.success) {
            this.employmentCountriesList = response.body;
            return response.body;
          } else {
            throw new Error(response.errorMessage);

          }
        })
      );
  }

  /*
   * Método que hace el request a los servicios para   
   * obtener el listado de los tipos de documento
   */
  documentTypes(): Observable<DropdownModel[]> {
    return this.http.get<GenericResponse<DropdownModel[]>>(`${this.baseURL}/document-types`)
      .pipe(
        map(response => {
          if (response.success) {
            this.documentTypesList = response.body;
            return response.body;
          } else {
            throw new Error(response.errorMessage);

          }
        })
      );
  }

  /*
   * Método que hace el request a los servicios para   
   * obtener el listado de las areas de empleo
   */
  employmentAreas(): Observable<DropdownModel[]> {
    return this.http.get<GenericResponse<DropdownModel[]>>(`${this.baseURL}/employment-areas`)
      .pipe(
        map(response => {
          if (response.success) {
            this.employmentAreasList = response.body;
            return response.body;
          } else {
            throw new Error(response.errorMessage);

          }
        })
      );
  }

}

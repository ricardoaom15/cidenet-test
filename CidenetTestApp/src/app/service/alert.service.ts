import { Injectable } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(
    private alertController: AlertController,
    public toastController: ToastController,
    ) { }

  async showConfirmationAlert(title: string, msg: string, okHandler: () => void) {
    const alert = await this.alertController.create({
      header: title,
      message: msg,
      buttons: [
        'Cancelar',
        {
          text: 'Confirmar',
          handler: okHandler
        }
      ]
    });
    await alert.present();
  }

  async showToast(msg: string, duration: number = 2000) {
    const toast = await this.toastController.create({
      message: msg,
      duration: duration
    });
    await toast.present();
  }
  
}

import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';
import { DropdownModel } from 'src/app/data/dropdown-model';
import { Employee } from 'src/app/data/model/employee';
import { GetEmployeesRq } from 'src/app/data/rq/get-employees-rq';
import { AlertService } from 'src/app/service/alert.service';
import { DropdownService } from 'src/app/service/dropdown.service';
import { InputUtil } from 'src/app/util/input-util';
import { EmployeeService } from '../../service/employee.service';

/*
 * Cantidad de elementos que se consultan cada vez 
 * que se consulta el listado de empleados
 */
const LIMIT: number = 10;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public employmentCountries: DropdownModel[] = [];
  public documentTypes: DropdownModel[] = [];
  public areas: DropdownModel[] = [];
  public employees: Employee[] = [];

  private requestNextPage: boolean = false;
  private lastSkip = -1;
  private skip = 0;

  public showFilters: boolean = false;
  private filterApplied: boolean = false;
  private filterEmployee: Employee = new Employee();

  constructor(
    private router: Router,
    private alertService: AlertService,

    private actionSheetController: ActionSheetController,


    private dropdownService: DropdownService,
    private employeeService: EmployeeService
  ) {
    this.initDropdowns();
  }

  /*
   * Método que limpia la lista de empleados
   */
  clearList() {
    this.lastSkip = -1;
    this.skip = 0;
    this.employees = [];
  }

  /*
   * Método que se llama cada vez que se entra en la pantalla, 
   * sirve para refrescar la vista cuando vuelve de la pantalla
   * de añadir empleado
   */
  ionViewDidEnter() {
    this.clearList();
    this.loadEmployees(undefined);
  }

  /*
   * Método que consulta los listados con los cuales se llenan los dropdowns para los filtros
   */
  initDropdowns() {
    this.dropdownService.employmentCountries().subscribe(response => { this.employmentCountries = response },
      error => { this.alertService.showToast("No se pudo cargar la información") });
    this.dropdownService.documentTypes().subscribe(response => { this.documentTypes = response },
      error => { this.alertService.showToast("No se pudo cargar la información") });
    this.dropdownService.employmentAreas().subscribe(response => { this.areas = response },
      error => { this.alertService.showToast("No se pudo cargar la información") });
  }

  /*
   * Método que consulta el listado de empleados
   */
  loadEmployees(event) {
    let request = new GetEmployeesRq();
    request.skip = this.skip;
    request.limit = LIMIT;
    if (this.filterApplied) request.filters = this.filterEmployee
    this.employeeService.findAllEmployees(request).subscribe(response => {
      this.requestNextPage = response.length == LIMIT;
      console.log("skip", this.skip, "lastSkip", this.lastSkip);
      if (this.skip != this.lastSkip) {
        Array.prototype.push.apply(this.employees, response);
      }
      if (event) {
        setTimeout(() => {
          event.target.complete();
        }, 500);
      }
    }, error => { this.alertService.showToast(error); });
  }

  /*
   * Método que consulta mas empleados cuando cuando el 
   * scroll llega al final
   */
  loadMoreEmployees(event) {
    this.lastSkip = this.skip;
    if (this.requestNextPage) this.skip += LIMIT;
    setTimeout(() => {
      this.loadEmployees(event);
    }, 1000);
  }

  /*
   * Método que consulta los empleados cuando se han 
   * aplicados filtros
   */
  searchEmployees() {
    this.filterApplied = true;
    this.showFilters = false;
    this.clearList();
    this.loadEmployees(undefined);
  }

  /*
   * Método que reinicia los filtros que se han aplicado
   */
  clearFilters() {
    this.filterApplied = false;
    this.showFilters = false;
    this.filterEmployee = new Employee()
    this.clearList();
    this.loadEmployees(undefined);
  }

  /*
   * Método que muestra el menu de cada elemento de la lista 
   * cuando se selecciona al hacer click/tap
   */
  async showItemMenu(employee: Employee, position: number) {
    const actionSheet = await this.actionSheetController.create({
      header: `Acciones para ${employee.name} ${employee.surname}`,
      buttons: [
        {
          text: "Editar empleado",
          icon: 'create-outline',
          handler: () => {
            this.navigateToEditEmployee(employee);
          }
        },
        {
          text: "Eliminar empleado",
          icon: 'trash',
          handler: () => {
            this.showDeleteAlert(employee, position);
          }
        }
      ]
    });
    await actionSheet.present();
  }

  /*
   * Método que realiza la navegación hacia la pantalla  
   * donde se registra un nuevo empleado
   */
  navigateToCreateEmployee() {
    this.employeeService.currentEmployee = undefined;
    this.router.navigate(['/save'])
  }

  /*
   * Método que realiza la navegación hacia la pantalla  
   * donde se actualiza la información de un empleado
   */
  navigateToEditEmployee(employee: Employee) {
    this.employeeService.currentEmployee = employee;
    this.router.navigate(['/save'])
  }

  /*
   * Método que muestra el mensaje de confirmación cuando   
   * se quiere eliminar un empleado
   */
  async showDeleteAlert(employee: Employee, position: number) {
    this.alertService.showConfirmationAlert(
      'Eliminar empleado',
      `¿Está seguro de eliminar el empleado ${employee.name} ${employee.surname}?\n Esta acción no se puede deshacer`,
      () => {
        this.deleteEmployee(employee, position);
      }
    );
  }

  /*
   * Método que consume el servicio que elimina el   
   * usuario seleccionado, posterior a confirmación
   */
  async deleteEmployee(employee: Employee, position: number) {
    this.employeeService.deleteEmployeeByDocument(employee.documentNumber, employee.documentType).subscribe(async response => {
      this.employees.splice(position, 1);
      this.alertService.showToast(response);
    }, error => {
      this.alertService.showToast(error);
    });
  }

  /*
   * Métodos que validan los caracteres que se puede ingresar en un Input 
   */
  allowAlphabetWithSpace(event) {
    InputUtil.allowAlphabetWithSpace(event);
  }

  allowDocumentChars(event) {
    InputUtil.allowDocumentChars(event);
  }

}

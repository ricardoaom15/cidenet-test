import { DatePipe, Location } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { DropdownModel } from 'src/app/data/dropdown-model';
import { Employee } from 'src/app/data/model/employee';
import { AlertService } from 'src/app/service/alert.service';
import { DropdownService } from 'src/app/service/dropdown.service';
import { EmployeeService } from 'src/app/service/employee.service';
import { InputUtil } from 'src/app/util/input-util';

@Component({
  selector: 'app-employee-save',
  templateUrl: './employee-save.page.html',
  styleUrls: ['./employee-save.page.scss'],
})
export class EmployeeSavePage {

  public employee: Employee;
  public isUpdate: boolean = false;
  public toolbarTitle: string = "Nuevo empleado";

  public employmentCountries: DropdownModel[] = [];
  public documentTypes: DropdownModel[] = [];
  public areas: DropdownModel[] = [];

  public saveEmployeeForm: FormGroup;
  public today: string;
  public lastMonthDay: string;
  public creationDate: string;
  public updateDate: string;

  constructor(
    public location: Location,
    public datePipe: DatePipe,
    public formBuilder: FormBuilder,
    private employeeService: EmployeeService,
    private dropDownService: DropdownService,
    private alertService: AlertService
  ) {
    this.initDatePickerOptions();
    this.initForm();
    this.initDropdowns();
    this.initEmployee();
  }

  /*
   * Método que consulta los listados con los cuales se llenan los dropdowns para los filtros
   */
  initDropdowns() {
    this.dropDownService.employmentCountries().subscribe(response => {
      this.employmentCountries = response
      this.saveEmployeeForm.get("employmentCountry").setValue(this.employee.employmentCountry);
    });
    this.dropDownService.documentTypes().subscribe(response => {
      this.documentTypes = response
      this.saveEmployeeForm.get("documentType").setValue(this.employee.documentType);
    });
    this.dropDownService.employmentAreas().subscribe(response => {
      this.areas = response
      this.saveEmployeeForm.get("area").setValue(this.employee.area);
    });
  }

  /*
   * Método que inicializa los componentes del formulario con las validaciones y valores inciales
   */
  initForm() {
    this.saveEmployeeForm = this.formBuilder.group({
      surname: ['', [Validators.required]],
      secondSurname: ['', [Validators.required]],
      name: ['', [Validators.required]],
      otherNames: ['', []],
      employmentCountry: [0, [Validators.required]],
      documentType: [0, [Validators.required]],
      documentNumber: ['', [Validators.required]],
      email: ['', [Validators.required]],
      admissionDate: [this.today, [Validators.required]],
      area: [0, [Validators.required]],
      status: ['Activo', [Validators.required]],
      creationDate: [this.creationDate, [Validators.required]],
      updateDate: [this.creationDate, [Validators.required]],
    })
  }

  /*
   * Método que inicializa los atributos para los date pickers
   */
  initDatePickerOptions() {
    let date = new Date();
    this.today = this.datePipe.transform(date, "yyyy-MM-dd");
    this.creationDate = this.datePipe.transform(date, "yyyy-MM-dd HH:mm:ss");
    this.updateDate = this.datePipe.transform(date, "yyyy-MM-dd HH:mm:ss");
    this.lastMonthDay = this.datePipe.transform(date.getTime() - (2592000000), "yyyy-MM-dd");
  }

  /*
   * Método que inicializa la información del usuario, validando si se va a agregar un 
   * usuario nuevo o se va a actulizar uno existente
   */
  initEmployee() {
    if (this.employeeService.currentEmployee) {
      this.isUpdate = true;
      this.employee = this.employeeService.currentEmployee;
      this.toolbarTitle = `Actualizar ${this.employee.name} ${this.employee.surname}`
      this.saveEmployeeForm.get("surname").setValue(this.employee.surname);
      this.saveEmployeeForm.get("secondSurname").setValue(this.employee.secondSurname);
      this.saveEmployeeForm.get("name").setValue(this.employee.name);
      this.saveEmployeeForm.get("otherNames").setValue(this.employee.otherNames);
      this.saveEmployeeForm.get("documentNumber").setValue(this.employee.documentNumber);
      this.saveEmployeeForm.get("email").setValue(this.employee.email);
      this.saveEmployeeForm.get("admissionDate").setValue(this.datePipe.transform(this.employee.admissionDate, "yyyy-MM-dd"));
      this.saveEmployeeForm.get("status").setValue(this.employee.status == 1 ? "Activo" : "Inactivo");
      this.saveEmployeeForm.get("creationDate").setValue(this.datePipe.transform(this.employee.creationDate, "yyyy-MM-dd HH:mm:ss"));
    } else {
      this.employee = new Employee();
    }
  }

  /*
   * Método que envia la informacion del usuario ya sea para registro o actualización
   */
  async saveEmployee() {

    if (this.saveEmployeeForm.valid) {
      this.employee.surname = this.saveEmployeeForm.get("surname").value.toUpperCase().trim();
      this.employee.secondSurname = this.saveEmployeeForm.get("secondSurname").value.toUpperCase();
      this.employee.name = this.saveEmployeeForm.get("name").value.toUpperCase();
      this.employee.otherNames = this.saveEmployeeForm.get("otherNames").value.toUpperCase().trim();
      this.employee.employmentCountry = this.saveEmployeeForm.get("employmentCountry").value;
      this.employee.documentType = this.saveEmployeeForm.get("documentType").value;
      this.employee.documentNumber = this.saveEmployeeForm.get("documentNumber").value;
      this.employee.email = this.saveEmployeeForm.get("email").value;
      this.employee.admissionDate = new Date(this.saveEmployeeForm.get("admissionDate").value).getTime();
      this.employee.area = this.saveEmployeeForm.get("area").value;
      this.employee.status = 1;
      this.employee.creationDate = new Date(this.saveEmployeeForm.get("creationDate").value).getTime();
      this.employee.updateDate = this.isUpdate ? new Date().getTime() : this.employee.creationDate;

      this.employeeService.saveEmployee(this.isUpdate, this.employee).subscribe(async response => {
        this.alertService.showToast(response);
        this.location.back();
      },error => { this.alertService.showToast(error); });
    } else {
      this.alertService.showToast("El formulario está incompleto");
    }
  }

  /*
   * Método que consulta el email valido para el usuario que se esta gestionando
   */
  buildEmail() {
    let name = this.saveEmployeeForm.get('name').value;
    let surname = this.saveEmployeeForm.get('surname').value;
    let employmentCountry = Number(this.saveEmployeeForm.get('employmentCountry').value);
    if (
      this.saveEmployeeForm.get('name').valid &&
      this.saveEmployeeForm.get('surname').valid &&
      this.saveEmployeeForm.get('employmentCountry').valid && employmentCountry != 0
    ) {
      this.employeeService.getValidEmail(name, surname, employmentCountry).subscribe(response => {
        this.saveEmployeeForm.get("email").setValue(response);
      })
    }
  }

  /*
   * Métodos que validan los caracteres que se puede ingresar en un Input 
   */
  allowAlphabet(event) {
    InputUtil.allowAlphabet(event);
  }

  allowAlphabetWithSpace(event) {
    InputUtil.allowAlphabetWithSpace(event);
  }

  allowDocumentChars(event) {
    InputUtil.allowDocumentChars(event);
  }

}

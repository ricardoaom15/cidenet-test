import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmployeeSavePage } from './employee-save.page';

const routes: Routes = [
  {
    path: '',
    component: EmployeeSavePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmployeeSavePageRoutingModule {}

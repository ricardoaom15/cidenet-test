import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EmployeeSavePageRoutingModule } from './employee-save-routing.module';

import { EmployeeSavePage } from './employee-save.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    EmployeeSavePageRoutingModule
  ],
  declarations: [EmployeeSavePage],
  providers: [DatePipe]
})
export class EmployeeSavePageModule { }

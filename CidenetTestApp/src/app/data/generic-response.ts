export class GenericResponse<T> {
    public success: boolean;
    public body: T;
    public errorMessage: string;

    constructor() { }
}
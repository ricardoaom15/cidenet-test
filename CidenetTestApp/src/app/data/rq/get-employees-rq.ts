import { Employee } from "../model/employee";

export class GetEmployeesRq {
    public skip: number;
    public limit: number;
    public filters?: Employee;

    constructor() { }
}
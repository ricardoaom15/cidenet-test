export class Employee {
    public documentNumber: string;
    public documentType: number;
    public surname: string;
    public secondSurname: string;
    public name: string;
    public otherNames: string;
    public employmentCountry: number;
    public email: string;
    public admissionDate: number;
    public area: number;
    public status: number;
    public creationDate: number;
    public updateDate: number;

    constructor() { 
        this.documentNumber = "";
        this.documentType = 0;
        this.surname = "";
        this.secondSurname = "";
        this.name = "";
        this.otherNames = "";
        this.employmentCountry = 0;
        this.email = "";
        this.admissionDate = new Date().getTime();
        this.area = 0;
        this.status = -1;
        this.creationDate = new Date().getTime();
        this.updateDate = new Date().getTime();
    }
}
export class DropdownModel {
    public id: number;
    public description: string;
    public abbreviation?: string;

    constructor() { }
}
export class InputUtil {

    public static allowAlphabet(event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
            event.preventDefault();
            return false;
        } else {
            return true;
        }
    }

    public static allowAlphabetWithSpace(event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122) && charCode != 32) {
            event.preventDefault();
            return false;
        } else {
            return true;
        }
    }

    public static allowNumbers(event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if ((charCode < 48 || charCode > 57)) {
            event.preventDefault();
            return false;
        } else {
            return true;
        }
    }

    public static allowDocumentChars(event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122) && (charCode < 48 || charCode > 57) && charCode != 45) {
            event.preventDefault();
            return false;
        } else {
            return true;
        }
    }

}
